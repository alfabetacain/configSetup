#find vim install location
function findInstallLoc() {
	param([Parameter(Mandatory=$true)][string]$key)
	$drives = get-psdrive -psprovider filesystem | %{$_.root}
	$folders = @("Program Files","Program Files (x86)")
	$installLoc = ""
	foreach ($drive in $drives) {
		foreach ($folder in $folders) {
			$path = "$drive\$folder"
			$path += $key
			if (test-path $path) {
				$installLoc = $path
				break
			}
		if ($installLoc -ne "") {
			break
		}
		}
	}
	return $installLoc
}
