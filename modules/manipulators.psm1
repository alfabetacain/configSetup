function appendToPath() {
	param([Parameter(Mandatory=$true)][string]$value)
	$oldPath = $env:path
	$env:path = $oldPath + ";" + $value + ";"
}
