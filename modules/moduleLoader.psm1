function loadModule() {
	param([Parameter(Mandatory=$true)][string]$module)
	if (-not (get-module $module)) {
		import-module $module -force
	}
}
