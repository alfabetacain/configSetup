﻿#import loader module if not accessible
if (-not (get-module "loader")) {
	import-module "..\modules\loader.psm1" -force
}
function enter() {
	param([Parameter(Mandatory=$true)][string]$path)
	if (-not (test-path $path)) {
		mkdir $path
	}
	cd $path
}
function cloneIntoBundle() {
	param([Parameter(Mandatory=$true)][string]$repo)
	$origLoc = get-location
	cd $home
	enter "vimfiles"
	enter "bundle"
	git clone $repo
	cd $origLoc
}
[string]$root = get-location #reference to execute location

#how to create hardlinks in powershell
#fsutil hardlink create <new-file> <existing-file>

loadModule finders.psm1
[string]$home = get-location
$vimLoc = findInstallLoc "Vim"
$vimrc = (get-item ".\_vimrc").fullname
cd $vimLoc
if (test-path ".\_vimrc") {
	del ".\_vimrc"
}
fsutil hardlink create "_vimrc" $vimrc

#setup vimfiles
cd $home
enter "vimfiles"
enter "autoload"
#download pathogen script
(new-object net.webclient).downloadstring("https://tpo.pe/pathogen.vim") > pathogen.vim
#pathogen#infect() should already be added in the _vimrc file
cd $root

#setup powershell syntax
cloneIntoBundle "https://github.com/PProvost/vim-ps1.git"

#setup nerdtree
cloneIntoBundle "https://github.com/scrooloose/nerdtree.git"

#setup nerdcommenter - use <leader>cc and <leader>cu
cloneIntoBundle "https://github.com/scrooloose/nerdcommenter.git"

#setup delimitMate for bracket autocompletion
cloneIntoBundle "https://github.com/Raimondi/delimitMate.git"

#setup solarized color scheme for windows terminals
#screw this, use ConEmu instead
# git clone git@github.com:neilpa/cmd-colors-solarized.git
# cd "cmd-colors-solarized"
# regedit /s solarized-dark.reg
# cd ..
# fsutil softlink create "solarized-dark" "C:\Windows\System32\WindowsPowershell\v1.0\powershell.exe"
# write-host "now you must open powershell through the shortcut, enter properties, edit something and click ok"
# write-host "Press enter when you are done"
# $null = Console.ReadKey()
# rmdir "cmd-colors-solarized" -force /y
# del "solarized-dark"

#setup solarized color scheme
cloneIntoBundle "git://github.com/altercation/vim-colors-solarized.git"

#remember to change this to make incremental search highlight work in con emu
write-host "Remember to change IncSearch in solarized.vim to look like this: `"exe "hi! IncSearch"      .s:fmt_stnd   .s:fg_base03 .s:bg_orange`""
