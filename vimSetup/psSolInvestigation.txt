Problem:
Powershell can be set up to use solarized color scheme on its
own. But using only this and not the solarized color scheme in vim
will result in different colors being used for highlighting.
Also using the vim color scheme will result in absurd highlighting
colors being chosen. 

Facts:
There seems to be some inconsistency between the colors that
solarized wants and the colors that powershell offers. 
Take for example import statements in java. 
In gVim the color chosen is orange. 
In powershell vim with only powershell itself solarized the 
chosen color is light blue. This is due to wrong mapping of colors. 
Orange is available, but not where vim thinks it is. 

Possible solutions:
1. Create custom vim syntax color file for future use
- Might create problems with new versions of vim. 
- Might be easier to create own color scheme. 
2. Modify solarized so to use the correct mappings
- Unsure if solarized does more than color mapping, might break something
- Unsure if goes against license
- Might need to ask creator, Ethan something
- Probably need to learn vimscript so I can understand the solarized color
  scheme internals
3. Create my own color scheme based on solarized with correct mappings
- This will most likely go against solarized license, need to ask creator

Useful commands:
For finding the used colors for a given highlight group ->
    :verbose hi <group name>

Cross reference with actual colors and expected colors. Expected can be seen
in the solarized.vim file in the solarized repo. 
