﻿if (-not (get-module "loader")) {
	import-module "..\modules\loader.psm1" -force
}
set-executionpolicy unrestricted

#download psget
(new-object Net.WebClient).DownloadString("http://psget.net/GetPsGet.ps1") | iex

#make sure that git/bin is on the path
loadModule finders.psm1
$gitInstall = findInstallLoc "Git"
$env:GIT_HOME = $gitInstall
loadModule manipulators.psm1
appendToPath "$gitInstall\bin"

#install posh-git
install-module posh-git
#reload profile for changes to take effect
. $profile
