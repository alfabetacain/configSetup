﻿$newProfile = (get-item "Microsoft.PowerShell_profile.ps1").fullname
if (test-path $profile) {
	del $profile
}
fsutil hardlink create $profile $newProfile
. $profile

#setup codeTemplates
function enter([string]$path) {
    if (-not (test-path $path)) {
            mkdir $path
    }
    cd $path
}

$root = get-location

#copy all the templates
copy "codeTemplates" -recurse $home
