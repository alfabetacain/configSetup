﻿#shortcuts
$eclipse = "C:\Program Files\eclipse"
$uni = "C:\uni"
$vimrc = "C:\Users\$([Environment]::UserName)\_vimrc"
$vimLib = "C:\Users\$([Environment]::UserName)\vimfiles"
$root = $home #for setting the current location to root
$scalaFolder = "$($uni)\scala"
$scalaExercies = "$($scalaFolder)\book\fpinscala\exercises\src\main\scala\fpinscala"
$scalaLib = "$($HOME)\scalaLib"
$zip = "C:\Program Files\7zip\7-Zip.exe"
$fslex = "C:\uni\FsLexYacc.6.1.0\build\fslex.exe"
$fsyacc = "C:\uni\FsLexYacc.6.1.0\build\fsyacc.exe"
$fsi = "C:\Program Files (x86)\Microsoft SDKs\F#\3.1\Framework\v4.0\fsi.exe"
$fsyaccRuntime = "C:\uni\FsLexYacc.Runtime.6.1.0\lib\net40\FsLexYacc.Runtime.dll"
Set-Alias fslex $fslex
Set-Alias fsyacc $fsyacc
Set-Alias fsi "C:\Program Files (x86)\Microsoft SDKs\F#\3.1\Framework\v4.0\fsi.exe"
$NotesLocation = "$home\notes"
$DefaultEditor = "vim"
if (-not (Test-Path $NotesLocation)) {
    mkdir $NotesLocation | Out-Null
}
Import-Module notes

$javadocLocation = "$home\javadoc\docs\api"
Import-Module jhelp

Import-Module git-helper-functions

# Load posh-git example profile
. 'C:\Users\christian\Documents\WindowsPowerShell\Modules\posh-git\profile.example.ps1'

function Size($w, $h)
{
    New-Object System.Management.Automation.Host.Size($w, $h)
}

function resize()
{
Write-Host '[Arrows] resize  [Esc] exit ...'
$ErrorActionPreference = 'SilentlyContinue'
for($ui = $Host.UI.RawUI;;) {
    $b = $ui.BufferSize
    $w = $ui.WindowSize
    switch($ui.ReadKey(6).VirtualKeyCode) {
        37 {
            $w = Size ($w.width - 1) $w.height
            $ui.WindowSize = $w
            $ui.BufferSize = Size $w.width $b.height
            break
        }
        39 {
            $w = Size ($w.width + 1) $w.height
            $ui.BufferSize = Size $w.width $b.height
            $ui.WindowSize = $w
            break
        }
        38 {
            $ui.WindowSize = Size $w.width ($w.height - 1)
            break
        }
        40 {
            $w = Size $w.width ($w.height + 1)
            if ($w.height -gt $b.height) {
                $ui.BufferSize = Size $b.width $w.height
            }
            $ui.WindowSize = $w
            break
        }
        27 {
            return
        }
    }
  }
}

#enter directory and list items
function cdd() {
    param([Parameter(Mandatory=$true)][string]$directory)
    cd $directory
    return dir
}

#cleans *~ files, vim backup files
function Clean-VimCopies() {
    param($directory)
    $root = get-location
    if ($directory -ne $null) {
        cd $directory
    }
    del *~
    dir | ? { $_.psiscontainer} | % { clean-vimcopies $_ }
    if ($directory -ne $null) {
        cd $root
    }
}

function Get-VimPlugins() {
    dir "$vimLib\bundle" | 
        % {
            $properties = @{"Name"="$($_.name)";"Last update"="$($_.lastwritetime)"}
            new-object -typename psobject -prop $properties
        }
}

function Make-Root() {
    $global:root = Get-Location
}

function Get-GitIgnoreDefault() {
    $text = "*~ `n"
    $text += "*.class `n"
    $text += "*.jar"
    return $text
}

function Setup-GitRepo() {
    $loc = get-location
    $stringPath = $loc.drive.currentlocation
    $parts = $stringPath.split("\")
    $dirName = $parts[$parts.length-1]
    "Repository for $dirName" > "readme.txt"
    $ignore = Get-GitIgnoreDefault
    ($ignore) > ".gitignore"
}

function Ping-Test() {
    param($indefinite=$false)
    if ($indefinite) {
        ping www.google.com -t
    }
    else {
        ping www.google.com
    }
}

function Speak() {
    param([Parameter(Mandatory=$true)][string]$text)
    Add-Type -AssemblyName System.Speech
    $speaker = New-Object System.Speech.Synthesis.SpeechSynthesizer
    $speaker.Speak($text)
}

function scalaTestExecute() {
    param([Parameter(Mandatory=$true)][string]$class)
    $testJar = ".;$($scalaLib)\scalatest_2.11-2.2.4.jar"
    scala -cp $testJar org.scalatest.run $class
}

function scalaTestCompile() {
    param([Parameter(Mandatory=$true)][string]$class)
    $testJar = ".;$($scalaLib)\scalatest_2.11-2.2.4.jar"
    fsc -classpath $testJar $class
}

function scalaTestRun() {
    param(
        [Parameter(Mandatory=$true)][string]$class,
        [switch]$clear
        )
    if ($clear) {
        cls
    }
    $file = Get-Item $class
    $associatedClass = "$($file.basename).class"
    if (-not (test-path $associatedClass) -or (Get-Item $associatedClass).lastWriteTimeUTC -lt $file.lastWriteTimeUTC) {
        scalaTestCompile ($file.name)
    }
    if ($lastexitcode -ne 0) {
        write-warning "Failed to compile"
        return
    }
    scalaTestExecute ($file.basename)
}

function Invoke-Template {
    param([string]$path="$($home)\codeTemplates",[Scriptblock]$scriptblock)

    function Get-Template {
        param($templateFileName)

        $content = [IO.File]::ReadAllText(
            (Join-Path $path $templateFileName))
        Invoke-Expression "@`"`r`n$content`r`n`"@"
    }

    & $scriptblock
}

function Make-ScalaTestStub {
    param([Parameter(Mandatory=$true)][string]$fileName)
    $result = Invoke-Template -scriptblock {
        $name = $fileName
        Get-Template ScalaTest.scala
    }
    $result | out-file -encoding ascii "$($fileName).scala"
}
# By Chad Miller - http://poshcode.org/2059
function Get-FileEncoding
{
    [CmdletBinding()] Param (
     [Parameter(Mandatory = $True, ValueFromPipelineByPropertyName = $True)] [string]$Path
    )

    [byte[]]$byte = get-content -Encoding byte -ReadCount 4 -TotalCount 4 -Path $Path

    if ( $byte[0] -eq 0xef -and $byte[1] -eq 0xbb -and $byte[2] -eq 0xbf )
    { Write-Output 'UTF8' }
    elseif ($byte[0] -eq 0xfe -and $byte[1] -eq 0xff)
    { Write-Output 'Unicode' }
    elseif ($byte[0] -eq 0 -and $byte[1] -eq 0 -and $byte[2] -eq 0xfe -and $byte[3] -eq 0xff)
    { Write-Output 'UTF32' }
    elseif ($byte[0] -eq 0x2b -and $byte[1] -eq 0x2f -and $byte[2] -eq 0x76)
    { Write-Output 'UTF7'}
    else
    { Write-Output 'ASCII' }
}

function di() {
    param(
        $path=".",
        [switch]$includeVim,
        [switch]$includeJava
        )
    $exclusions = @()
    if (-not $includeJava) {
        $exclusions += @("*.class")
    }
    if (-not $includeVim) {
        $exclusions += @("*~")
    }
    Get-Childitem -path $path -exclude $exclusions
}

function Guess-NumberGame() {
    param(
       [Parameter(Mandatory=$true)][int]$max 
        )
    if ($max -lt 0) {
        write-warning "Maximum must be larger than 0"
        return
    }

    $number = Get-Random $max
    while ($true) {
        Write-Host "Please input your guess"
        $line = Read-Host
        try {
            $value = [Int]::Parse($line)
            if ($value -lt $number) {
                write-host "Target is larger"
            }
            elseif ($value -gt $value) {
                write-host "Target is lower"
            }
            else {
                Write-Host "Correct!!!"
                return
            }
        }
        catch [Exception] {
            if ($line.equals(":quit")) {
                Write-Host "Qutting..."
                return
            }
            Write-Warning "Failed to read `"$line`" as int. Please try again or type :quit to exit"
        }
    }
}

function Change-FileEncoding() {
    param(
        [Parameter(Mandatory=$true)][string]$file,
        [string]$encoding="ascii",
        [string]$newName=""
        )
    if (-not (Test-Path $file)) {
        Write-Warning "File $file not found!"
        return
    }
    $c = Get-Content $file
    if ($newName -ne "") {
        $file = $newName
    }
    $c | Out-File -encoding $encoding $file
}

#make thrash function that automatically recursively deletes a directory and puts it into trash
